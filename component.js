import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import './ClientMainPage.scss';
import Loader from '../../Loader/Loader';
import RenderField from '../../LogicBlocks/RenderField/RenderField';
import ClientHeader from '../../../components/Client/ClientHeader/ClientHeader';

import { required, email } from '../../../helpers/validation';
import { getPayment, getBtcUsdRate, postPaymentConfirm } from '../../../actions/userActions';

class ClientMainPage extends Component {
    state={
        error: null,
        loader: false
    };

    componentWillMount() {
        const {match} = this.props;
        let id = match.params.id;
        this.props.getPayment(id);
        this.props.getBtcUsdRate();
    };

    SubmitForm=(data)=>{
        this.props.main.error=[];
        this.setState({loader: true});
        const { postPaymentConfirm, history } = this.props;
        const {match} = this.props;
        let id = match.params.id;
        postPaymentConfirm(id, data).then((res)=>{
            if(res.payload && res.payload.status && res.payload.status == 200 || res.payload && res.payload.status && res.payload.status == 201 ){
                this.setState({loader: false});
                history.push(`/client/${id}/payment`);
            } else {
                this.setState({loader: false});
            }
        });
    };

    getError=(error)=>{
        let message = [];
        for (let key in error) {
            message.push(error[key]);
        }
        return(
            message.map((el, i)=>{
                return (
                    <p key={i}>{el}</p>
                )
            })
        )
    };

    render(){
        const { handleSubmit, submitting, shop:{payment}, main:{btc_usd_rate} } = this.props;
        if(!payment || payment && !payment.store_name || !btc_usd_rate || btc_usd_rate && !btc_usd_rate.rate) return <Loader single={true}/>;
        return (
            <div>
                <div className="container client-container">
                    <ClientHeader/>
                    <div className="client-page-container">
                        <div className="client-offer-box border-box">
                            <div className="offer-container">
                                <div>
                                    <h1 className="main-caption">Shop Payment</h1>
                                    <h2  className="sub-caption">You want to make a purchase in the shop "Shop Payment"</h2>
                                </div>
                                <a href={payment.contacts_link} className="btn btn-blue">Contact seller</a>
                            </div>
                        </div>
                        <form  onSubmit={handleSubmit((data)=>{this.SubmitForm(data)})}>
                            <div className="client-profile-box">
                                <div className="customer-info border-box">
                                    <h1 className="main-caption">Customer Information</h1>

                                    <div className="input-container">
                                        <div className="input-half">
                                            <Field name="customer_first_name" type="text" validate={[required]} component={RenderField} label="FIRST NAME" autoComplete='off'/>
                                        </div>
                                        <div className="input-half">
                                            <Field name="customer_last_name" type="text" validate={[required]} component={RenderField} label="LAST NAME" autoComplete='off'/>
                                        </div>
                                    </div>
                                    <div className="input-container">
                                        <div className="input-full">
                                            <Field name="customer_email" type="text" validate={[required, email]} component={RenderField} label="EMAIL" autoComplete='off'/>
                                        </div>
                                    </div>
                                </div>
                                <div className="summary-info">
                                    <h1 className="high-caption">Summary</h1>
                                    <hr className="summary-hr"/>
                                    <div className="summary-info-item">
                                        <p>Total</p>
                                        <p>$ {Math.floor(Number(payment.amount_usd) * 100)/100}</p>
                                    </div>
                                    <div className="summary-info-item">
                                        <p>Total Price in Coin</p>
                                        <p className="coin-total-price"><img src="../../../../assets/img/icon-btc.png"/>{Math.floor((Number(payment.amount_usd)/btc_usd_rate.rate) * 1000000)/1000000}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="bottom-client-options">
                                <a href={payment.store_url} className="return-link"><span></span>Return to the seller's store</a>
                                <div className="center-btn">
                                    <button type="submit" className="btn btn-blue">Continue to Payment</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="terms-and-conditions-block">
                    <p>By clicking this button, you agree to <span className="terms-link">Terms and conditions</span></p>
                </div>
            </div>
        );
    }
}



ClientMainPage = reduxForm({
    form: 'ClientMainPage',
})(ClientMainPage);

function mapStateToProps(state, props) {
    return{
        main: state.main,
        shop: state.shop
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getPayment,
        getBtcUsdRate,
        postPaymentConfirm
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClientMainPage);